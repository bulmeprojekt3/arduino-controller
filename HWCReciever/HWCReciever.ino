#include <SoftwareSerial\SoftwareSerial.h>

SoftwareSerial bluetooth(7, 8);

void setup() {
	pinMode(A0, INPUT);
	pinMode(A1, INPUT);
	pinMode(A2, INPUT);
	pinMode(A3, INPUT);
	pinMode(2, INPUT);
	pinMode(3, INPUT);
	bluetooth.begin(9600);
	Serial.begin(38400);
}
//at+pair=98d3,31,204752,9

uint8_t buffer[5];


//bluetoothController.writeBytes(new byte[]{(byte) 2, (byte) (lastServo >> 8), (byte) (lastServo & 0xFF)});

int count = 0;
int lServo = 0;
int lSpeed = 0;
int lr = 0;
int lg = 0;
int lb = 0;

void loop() {
	int x = analogRead(A0);
	int y = analogRead(A1);
	
	int r = map(analogRead(A3),0,1023,0,255);
	int g = map(analogRead(A4), 0, 1023, 0, 255);
	int b = map(analogRead(A5), 0, 1023, 0, 255);

	int servo = map(x, 0, 1023, 900, 1460);
	int speed = map(y, 0, 1023, 100, 0 - 100);

	//Serial.println(servo);
	count++;
	if (count > 20){
		if (lServo != servo){
			buffer[0] = 2;
			buffer[1] = servo >> 8;
			buffer[2] = servo & 0xFF;

			bluetooth.write(buffer, 3);
			lServo = servo;
		}
		if (lSpeed != speed){
			buffer[0] = 1;
			buffer[1] = speed < 0 ? -1 : 1;
			buffer[2] = abs(speed);

			bluetooth.write(buffer, 3);
			lSpeed = speed;
		}
		if (lr != r||lb!=b||lb!=g){
			lr = r;
			lg = g;
			lb = b;
			buffer[0] = 0;
			buffer[1] = 4;
			buffer[2] = 255 - r;
			buffer[3] = 255 - g;
			buffer[4] = 255 - b;
			bluetooth.write(buffer, 5);
		}
		count = 0;
	}
	//1460 links
	//1200 ausgangs position
	//900 rechts

	if (bluetooth.available() > 0){
		Serial.write(bluetooth.read());
	}
	if (Serial.available() > 0){
		int data = Serial.read();
		bluetooth.write(data);
	}
	/*
	Serial.print("R: ");
	Serial.print(analogRead(A3));
	Serial.print(" G: ");
	Serial.print(analogRead(A4));
	Serial.print(" B: ");
	Serial.println(analogRead(A5));*/
	delay(1);
}
